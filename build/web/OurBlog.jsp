<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Our Blog</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/ourBlog.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeaderBlog").load("Headers/MasterHeader.jsp");
            });
            $(function () {
                $("#ourFooterBlog").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeaderBlog"></div>
        <div class="container second">
            <div class="row">
                <!-- Left SECTIOn -->
                <div class="col-md-8">
                    <div class="hair-transplant">
                        <div class="left">
                            <h4 class="card-title">HAIR TRANSPLANT, SPECIAL RAMADAN OFFER</h4>
                            <p class="card-text">BY ADMIN<br>03 MAY,2018<br>HAIR TREATMENT
                                <br>50 PERCENT, HAIR TRANSPLANT<br>OFFER, RAMADAN OFFER, RAMADAN DISCOUNT</p>
                            <div class="text-center">
                                <img src="images/left.png" width="96%" height="auto">
                            </div>
                            <h4 class="card-bottom">Ramadan offer 50 percent discount<br>Hair Transplant Laforma Clinic Lahore Pakistan</h4> 
                        </div>
                        <div class="third-b">
                            <h4 class="title">Hair Transplant, Special Ramadan Offer</h4>
                            <p class="para">Hair Transplant, Special Ramadan Offer . 50% Discount</p>
                            <h4 class="title-1">FOR APPOINTMENT:</h4>
                            <p class="para">042-35236070, 0300-3446070<br>Whatsapp: 0300-9671600</p>
                            <h4 class="title-1">For more information regarding<br>Hair Transplant</h4>
                            <p class="para">http://laformaclinic.com/hair-transplant-lahore-pakistan/<br>Facebook: www.facebook.com/laformaclinic</p>
                        </div> 
                        <button class="btn-1">MORE</button>
                    </div>
                    <div class="facelift">
                        <div class="left">
                            <h4 class="card-title">3D HIFU Face, Non-Surgical Facelift</h4>
                            <p class="card-text">BY ADMIN<br>02 May, 2018<br>FACE</p>
                            <div class="text-center">
                                <img src="images/facelift.png" width="96%" height="auto">
                            </div>
                            <h4 class="card-bottom">Ramadan offer 50 percent discount<br>Hair Transplant Laforma Clinic Lahore Pakistan</h4> 
                        </div>
                        <div class="third-b">

                            <p class="para">Put aging skin back in its place without any surgery.<br>Sagging skin gives you tired-looking eyes and distorts<br>the contours of your face at the chin and neck.</p>

                            <p class="para">3D High-intensity focused ultrasound lifts and tightens<br>skin like no other.</p>
                            <h4 class="title-1">Book your appointment now and avail 25% off on the treatment<br><br>Only trust 3D HiFU for Dynamic results</h4>
                            <p class="para">*Call (042) 35236070 to book this amazing offer!!! *<br>Whatsapp 0300-3446070</p>
                        </div> 
                        <button class="btn-1">MORE</button>
                    </div>
                    <div class="breast-size">
                        <div class="left">
                            <h4 class="card-title">A Decade of Boosting Breast Size</h4>
                            <p class="card-text">BY ADMIN<br>03 MAY,2018<br>BREAST<br>BREAST AUGMENTATION, WOMEN
                            </p>
                            <div class="text-center">
                                <img src="images/breast-size.png" width="96%" height="auto">
                            </div>
                            <h4 class="card-bottom">Breast implant operations have surged<br>40 percent in the past decade, with nearly 300,000 women<br>last year opting to increase their breast size.</h4> 
                        </div>
                        <button class="btn-1">MORE</button>
                    </div>
                </div>
                <!-- END Left-SECTION -->

                <!-- Start right SECTION -->
                <div class="col-md-4">
                    <div class="search">
                        <input type="text" class="form-control input-sm" maxlength="64" placeholder="Search" />
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </div>
                    <br><br>
                    <div class="right-list">
                        <ul class="list">
                            <li>Hair Transplant, Special Ramadan Offer</li>
                            <li>3D HIFU Face, Non-Surgical Facelift</li>
                            <li>A Decade of Boosting Breast Size</li>
                            <li>The Foolproof Way to Control Your Portions Every Time</li>
                            <li>Latest Health Lessons</li>

                            <li>Admin on Breast augmentation with Fat injection</li>
                            <li>Admin on Piercing</li>
                            <li>Admin on Breast augmentation with Fat injection</li>
                            <li>Ruma Nadeem on Breast augmentation with Fat injection</li>
                            <li>FirstRandal on Breast augmentation with Fat injection</li>
                        </ul>
                    </div>
                    <div class="right-list1">
                        <ul class="list">
                            <li>May 2018</li>
                            <li>March 2015</li>
                            <li>September 2012</li>
                        </ul>
                    </div>
                    <div class="right-list1">
                        <ul class="list">
                            <li>Breast</li>
                            <li>Face</li>
                            <li>Fitness</li>
                            <li>Hair Treatment</li>
                            <li>Health Tips</li>

                            <li>Uncategorized</li>
                            <li>Weight Loss</li>
                        </ul>
                    </div>
                    <div class="right-list1">
                        <ul class="list">
                            <li>Register</li>
                            <li>Log in</li>
                            <li>Entries RSS</li>
                            <li>Comments RSS</li>
                            <li>WordPress.org</li>
                        </ul>
                    </div>
                </div>
                <!-- End Right Section -->

            </div>
            <!-- End Description -->
            <div id="ourFooterBlog"></div>
        </div> 
        <!-- END CONTENT -->
    </body>
</html>
