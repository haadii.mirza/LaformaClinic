
<html>
    <head>
        <title>Hair Transplant</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/MALE_Hair_Transplant_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/hairTransplant.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Hair Transplant in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="hair-transplant"></div>
            </div>
            <div class="row third-row">

                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Hair Transplant Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivHairTransIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">What Is a Hair Transplant?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivHairTransSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivHairTransThird"> 
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>

