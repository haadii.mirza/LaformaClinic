
<html>
    <head>
        <title>Breast Lift</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BREAST_Breast_Lift_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/breastLift.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Breast lift in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="breastlift"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivBreastLiftIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Breast lift Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastLiftSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is Breast lift performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastLiftThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastLiftFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation<br>for Breast lift with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastLiftFifth"> 
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
