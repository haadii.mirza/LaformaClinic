<%-- 
    Document   : AdminDashboardNavbarAndNavigation
    Created on : Jun 22, 2018, 3:20:24 PM
    Author     : Nano Info 3
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin | LaFormaClinic</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="assests/vendor/AdminPanelAssests/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/AdminPanelAssests/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/vendor/AdminPanelAssests/js/adminlte.js" type="text/javascript"></script>
        <script src="assests/js/AdminEditContents.js" type="text/javascript"></script>
        <!--<script src="assests/js/SideBar_Menu_And_SubMenu_Loading_Requests.js" type="text/javascript"></script>-->
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/vendor/AdminPanelAssests/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
        <link href="assests/vendor/AdminPanelAssests/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assests/vendor/AdminPanelAssests/css/ionicons.css" rel="stylesheet" type="text/css"/>
        <link href="assests/vendor/AdminPanelAssests/css/_all-skins.css" rel="stylesheet" type="text/css"/>

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .myImageSize{
                height: 200px;
                width:200px;
                display: inline-table;
            }
            div > .myCustomButton {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-77%, 97%);
                -ms-transform: translate(-50%, -50%);
                background-color: black;
                color: white;
                font-size: 16px;
                padding: 12px 24px;
                border: none;
                cursor: pointer;
                border-radius: 49px;
                text-align: center;
            }
            div > .mybtn {
                top: 50%;
                left: 50%;
                transform: translate(-77%, 97%);
                -ms-transform: translate(-50%, -50%);
                background-color: black;
                color: white;
                font-size: 16px;
                padding: 12px 24px;
                border: none;
                cursor: pointer;
                border-radius: 49px;
                text-align: center;
            }

            div > .myCustomButton:hover {
                background-color: red;
                color: white;
            }
            div > .myCustomButton:visited{
                background-color: black;
                color: white;
            }
            div > .myCustomButton:active{
                background-color: black;
                color: white;
            }


            //image plugin classes
            * {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            .content-wrapper {
                background-color: #e0e0e0;
            }
            #page {
                text-align: center;
                font-size: 16px;
                margin-top:84px;
            }
            #page h1 {
                margin-bottom: 4rem;
                font-family: 'Lemonada', cursive;
                text-transform: uppercase;
                font-weight: normal;
                color: #000;
                font-size: 2rem;
            }

            /*** CUSTOM FILE INPUT STYE ***/
            .wrap-custom-file {
                position: relative;
                display: inline-block;
                width: 150px;
                height: 150px;
                margin: 0 0.5rem 1rem;
                text-align: center;
            }
            .wrap-custom-file input[type="file"] {
                position: absolute;
                top: 0;
                left: 0;
                width: 2px;
                height: 2px;
                overflow: hidden;
                opacity: 0;
            }
            .wrap-custom-file label {
                z-index: 1;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                right: 0;
                width: 100%;
                overflow: hidden;
                padding: 0 0.5rem;
                cursor: pointer;
                background-color: #fff;
                border-radius: 4px;
                -webkit-transition: -webkit-transform 0.4s;
                transition: -webkit-transform 0.4s;
                transition: transform 0.4s;
                transition: transform 0.4s, -webkit-transform 0.4s;
            }
            .wrap-custom-file label span {
                display: block;
                margin-top: 2rem;
                font-size: 1.4rem;
                color: #777;
                -webkit-transition: color 0.4s;
                transition: color 0.4s;
            }
            .wrap-custom-file label .fa {
                position: absolute;
                bottom: 1rem;
                left: 50%;
                -webkit-transform: translatex(-50%);
                transform: translatex(-50%);
                font-size: 1.5rem;
                color: lightcoral;
                -webkit-transition: color 0.4s;
                transition: color 0.4s;
            }
            .wrap-custom-file label:hover {
                -webkit-transform: translateY(-1rem);
                transform: translateY(-1rem);
            }
            .wrap-custom-file label:hover span, .wrap-custom-file label:hover .fa {
                color: #333;
            }
            .wrap-custom-file label.file-ok {
                background-size: cover;
                background-position: center;
            }
            .wrap-custom-file label.file-ok span {
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                padding: 0.3rem;
                font-size: 1.1rem;
                color: #000;
                /*background-color: #dbe1e8;*/
            }
            .wrap-custom-file label.file-ok .fa {
                display: none;
            }
            .mybuttonCss{
                margin-left: 520px;
            }
        </style>
    </head>
    <!-- ADD THE CLASS sidebar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
    <body class="hold-transition skin-blue fixed sidebar-mini">
        <!-- Site wrapper -->
        <!--<div class="wrapper">-->
        <div class="adminPanelHeader"></div>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <div class="adminPanelSideBar"></div>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="container">
                <div class="col-md-12" style="height:500px;">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="./saveImage.si" method="post" enctype="multipart/form-data">
                                <!--                                                    <div class="form-group">
                                                                                        <div class="file-loading">
                                                                                            <input type="file" name="file" multiple>
                                                                                        </div>
                                                                                    </div>-->
                                <div id="page">
                                    <header>
                                        <h1>Select the files to Upload</h1>
                                    </header>

                                    <!-- Our File Inputs -->
                                    <div class="col-md-2">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="file" id="image1" accept=".gif, .jpg, .png" />
                                            <label  for="image1">
                                                <span>Select Image One</span>
                                                <i class="fa fa-plus-circle"></i>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="file" id="image2" accept=".gif, .jpg, .png" />
                                            <label  for="image2">
                                                <span>Select Image Two</span>
                                                <i class="fa fa-plus-circle"></i>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="file" id="image3" accept=".gif, .jpg, .png" />
                                            <label  for="image3">
                                                <span>Select Image Three</span>
                                                <i class="fa fa-plus-circle"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="file" id="image4" accept=".gif, .jpg, .png" />
                                            <label  for="image4">
                                                <span>Select Image Four</span>
                                                <i class="fa fa-plus-circle"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="file" id="image5" accept=".gif, .jpg, .png" />
                                            <label  for="image5">
                                                <span>Select Image five</span>
                                                <i class="fa fa-plus-circle"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="wrap-custom-file">
                                            <input type="file" name="file" id="image6" accept=".gif, .jpg, .png" />
                                            <label  for="image6">
                                                <span>Select Image six</span>
                                                <i class="fa fa-plus-circle"></i>
                                            </label>
                                        </div>
                                        <!-- End Page Wrap -->
                                    </div>
                                </div>
                                <div >
                                    <button type="submit" class="mybtn mybuttonCss ">Upload</button>
                                </div>
                            </form>
                        </div>    
                    </div>
                </div>
            </div>
            <hr>
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 myImageList">

                        </div>    
                    </div>
                </div>
            </div>
            <!-- Main content -->
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!--            <footer class="main-footer">
                        <div class="pull-right hidden-xs">
                            <b>Version</b> 2.4.0
                        </div>
                        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                        reserved.
                    </footer>-->

        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <!-- <div class="control-sidebar-bg"></div>
      </div> -->
        <!-- ./wrapper -->

        <!-- jQuery 3 -->

        <script>
            $(document).ready(function () {
                // *****START **** populating the List of Roles stored in DB 
                $.ajax({
                    type: "GET",
                    dataType: 'json',
                    url: "/LaFormaClinic/fetchImagesToDelete.si",
                    success: function (data) {
                        console.log("success");
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            var URL = data[i].URL;
                            //$('.mySliderClass').append('<div class="item"><img src="uploads/' + URL + '" alt=""></div>');

                            $('.myImageList').append('<div class="item col-md-2"><img src="uploads/' + URL + '" alt="" class="myImageSize"><button class="myCustomButton" id="myButton" data="' + URL + '">Delete</button></div>');
                        }
                    },
                    error: function () {
                        console.log("failure");
                    }
                });
                // *****END **** populating the List of Roles stored in DB 

                $('body').on('click', '#myButton', function () {
                    var fileName = $(this).attr('data');
                    //alert(fileName);
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: "/LaFormaClinic/DeleteSliderImage.si?id=" + fileName + "",
                        success: function (data) {
                            window.location.replace("/LaFormaClinic/forwardToPanel.si");
                        },
                        error: function () {
                            console.log("failure");
                        }
                    });
                });
                ///images plugin js

                $('input[type="file"]').each(function () {
                    // Refs
                    var $file = $(this),
                            $label = $file.next('label'),
                            $labelText = $label.find('span'),
                            labelDefault = $labelText.text();
                    // When a new file is selected
                    $file.on('change', function (event) {
                        var fileName = $file.val().split('\\').pop(),
                                tmppath = URL.createObjectURL(event.target.files[0]);
                        //Check successfully selection
                        if (fileName) {
                            $label
                                    .addClass('file-ok')
                                    .css('background-image', 'url(' + tmppath + ')');
                            $labelText.text(fileName);
                        } else {
                            $label.removeClass('file-ok');
                            $labelText.text(labelDefault);
                        }
                    });
                    // End loop of file input elements
                });
            });//ending main Funtion for Jquery
        </script>

    </body>
</html>
