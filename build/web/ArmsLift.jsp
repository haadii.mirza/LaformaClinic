<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
    <head>
        <title>ArmsLifts</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BODY_Arms_Lift_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/armsLift.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
</head>
<body>
    <div id="ourHeader"></div>
    <div class="container first">
        <div class="row first-row">
            <div class="col-md-12 heading">
                <h2>ARMS LIFTS (Brachioplasty) in Lahore, Pakistan</h2>
            </div>
        </div>
        <div class="row second-row">
            <div class="arms"></div>
        </div>
        <div class="row third-row">
            <div class="col-md-12 myDivArmsLiftIntro"> 
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2 id="orange">Arm lift in lahore</h2>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12 myDivArmsLiftSecond"> 
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2 id="purple">How is arm lift performed?</h2>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12 myDivArmsLiftThird"> 
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2 id="orange">What to expect after surgery?</h2>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12 myDivArmsLiftFourth"> 
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2 id="purple">Schedule a consultation<br>for arm lift in lahore with Dr. Farrukh!</h2>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12 myDivArmsLiftFifth"> 
            </div>
        </div>
        <div id="ourFooter"></div>
    </div>
</body>
</html>
