<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Post Bariatric Body Lift</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BODY_Post_Bariatic_Body_Lift_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/post-Bariatric-Body-lift.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Post bariatric body Lift in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="body"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivPostBariaticIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Post Bariatric Body Lift in lahore?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivPostBariaticSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is post bariatric body lift performed?</h2>
                    <p>Dr. Farrukh may recommend a combination of the following procedures:</p>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 img-align">
                    <div class="col-md-1"></div>
                    <div class="col-md-2  ">
                        <img class="image" src="images/pos-1.png" alt="" width="170px" />
                    </div>
                    <div class="col-md-2">
                        <img class="image" src="images/pos-2.png" alt="" width="170px"/>
                    </div>
                    <div class="col-md-2 ">
                        <img class="image" src="images/pos-3.png" alt="" width="170px"/>
                    </div>
                    <div class="col-md-2">
                        <img  class="image" src="images/pos-4.png" alt="" width="170px"/>
                    </div>
                    <div class="col-md-2 ">
                        <img class="image" src="images/pos-5.png" alt="" width="170px"/>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="col-md-12 myDivPostBariaticThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivPostBariaticFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivPostBariaticFifth"> 
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
