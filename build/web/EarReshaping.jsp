<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Ear Reshaping</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/FACE_Ear_Reshaping_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/earReshaping.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Ear Reshaping in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="ear"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivEarReshapeIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Ear Piercing Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivEarReshapeSecond">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Is the ear piercing experience painful?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivEarReshapeThird">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivEarReshapeFourth">
                    <p></p>
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
