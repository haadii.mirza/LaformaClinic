
<html>
    <head>
        <title>Botox</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/NON_SURGICAL_Botox_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/botox.css" rel="stylesheet" type="text/css"/>
        <script> 
     $(function(){ $("#ourHeader").load("Headers/NavBarHeader.jsp"); });
     $(function(){ $("#ourFooter").load("Footers/MainFooter.jsp"); });
   </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Botox in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="botox"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivBotoxIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Botox Lahore</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBotoxSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is procedure performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBotoxThird"> 
            </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after botox treatment?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBotoxFourth"> 
            </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBotoxFifth"> 
            </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
