
<html>
    <head>
        <title>PRP Surgery</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/contactUs.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Contact Us</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="col-md-12 map">
                    <iframe class="mapid" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3403.1030302423032!2d74.2914149145294!3d31.46635185695712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3919015f37cb779b%3A0xa7b82ae78180d557!2sLa&#39;forma+Specialist+Clinic!5e0!3m2!1sen!2s!4v1528138388522" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <br>
            <div class="row third-row">
                <div class="col-md-12">
                    <p>La'forma Plastic, Cosmetic and Hair Transplant Surgery Clinic. 41-B, Kayaban-e-Firdousi, Johar
                        Town, Lahore. For<br>Consultation: (+92) 042-35236070, (+92) 0300-3446070 </p>
                </div>

                <p id="comment">Leave a Reply</p>
                <p id="purple-pp">Your email address will not be published. Required fields are marked *</p>
                <div class="col-md-8 fill">
                    <div class="col-md-6">
                        <input type="text" placeholder="NAME*" name=""></div>
                    <div class="col-md-6">
                        <input type="text" placeholder="PHONE NO*" name="">
                    </div></<br>
                    <div class="col-md-6">
                        <input type="text" placeholder="EMAIL*" name=""> 
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="SUBJECT" name="">
                    </div><br>
                    <div class="col-md-12">
                        <textarea rows="10" cols="50">MESSAGE</textarea><br>
                        <button id="btn">POST COMMENT</button></div>
                </div>

                <!--                    <textarea rows="10" cols="106">MESSAGE</textarea><br>
                                     <button id="btn">POST COMMENT</button>-->
            </div>

            <div id="ourFooter"></div>
        </div>
    </body>
</html>