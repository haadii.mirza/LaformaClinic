
<html>
    <head>
        <title>Breast Reconstruction</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BREAST_Breast_Reconstruction_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/breastReconstruction.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Breast Reconstruction in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="breast-reconstruction"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivBreastReconIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Breast Reconstruction Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastReconSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is it performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastReconThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">WHAT TO EXPECT AFTER SURGERY?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastReconFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastReconFifth"> 
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
