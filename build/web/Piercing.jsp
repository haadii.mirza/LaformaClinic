<html>
    <head>
        <title>Piercing</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/piercing.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Piercing</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="piercing"></div>
            </div>
            <hr>
            <p id="comment">Leave a Reply </p>
            <p id="purple-pp">Your email address will not be published. Required fields are marked *</p>
            <div class="fill">
                <input type="text" placeholder="NAME*" name=""><br>  
                <input type="text" placeholder="EMAIL*" name=""> <br>
                <input type="text" placeholder="WEBSITE" name=""><br>
            </div>
            <textarea rows="10" cols="50">COMMENT</textarea><br>
            <button id="btn">POST COMMENT</button>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>