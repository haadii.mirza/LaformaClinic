
<html>
    <head>
        <title>Medical Tourism</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/OFFERS_MedicalTourism_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/medicalTourism.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <!--            <div class="row first-row">
                            <div class="col-md-12 heading">
                                <h2>Brow lift in Lahore, Pakistan</h2>
                            </div>
                        </div>-->
            <div class="row second-row">
                <div class="tour"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivMedTourIntro">
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <img src="images/tour-1.png" alt="" width="100%"/>
                    </div>
                    <div class="col-md-6">
                        <img src="images/tour-2.png" alt="" width="100%"/>
                    </div>
                </div>
                <div class="col-md-12 gap">
                    <div class="col-md-6">
                        <img src="images/tour-3.png" alt="" width="100%"/>
                    </div>
                    <div class="col-md-6">
                        <img src="images/tour-4.png" alt="" width="100%" height="423px"/>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Medical tourism Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivMedTourSecond">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Welcome to Laforma Plastic Surgery</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivMedTourThird">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Step One: The Initial Contact<br>Fly-In For a Personal Consultation and Exam</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivMedTourFourth">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Or Receive a Virtual Consultation</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivMedTourFifth">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Scheduling</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivMedTourSixth">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Recovery</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivMedTourSeventh">

                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
