/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FaceLiftNeckLift;

import LipReduction.*;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class LipReducController {

    LipReducDao fDao = new LipReducDao();
    LipReducHandler fh;

    @RequestMapping(value = "/getFlNlPageFaceLiftNeckLiftIntro.flnl", method = RequestMethod.GET)
    public void doGet(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_FlNlIntro(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getFlNlPageFaceLiftNeckLiftSecond.flnl", method = RequestMethod.GET)
    public void FlNlsecond(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_FlNlSecond(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getFlNlPageFaceLiftNeckLiftThird.flnl", method = RequestMethod.GET)
    public void FlNlThird(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_FlNlThird(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getFlNlPageFaceLiftNeckLiftFourth.flnl", method = RequestMethod.GET)
    public void FlNlFourth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_FlNlFourth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
     @RequestMapping(value = "/getFlNlPageFaceLiftNeckLiftFifth.flnl", method = RequestMethod.GET)
    public void FlNlfifth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_FlNlFifth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
}
