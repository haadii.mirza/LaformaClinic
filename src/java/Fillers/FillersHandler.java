/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fillers;

/**
 *
 * @author Nano Info 3
 */
public class FillersHandler {
    private String id;
    private String PageId;
    private String name;
    private String Content ;

    public String getPageId() {
        return PageId;
    }

    public void setPageId(String PageId) {
        this.PageId = PageId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }
    
}
