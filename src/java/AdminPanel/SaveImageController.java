/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminPanel;

import com.google.gson.Gson;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class SaveImageController {

    SaveImageDao SIDAO = new SaveImageDao();
    UserHandler user;
    private final String UPLOAD_DIRECTORY = "F:/LaFormaClinic/web/uploads";

    @RequestMapping(value = "/forwardToPanel.si", method = RequestMethod.GET)
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/AdminDashboardNavbarAndNavigation.jsp").forward(request, response);
    }

    @RequestMapping(value = "/checkUser.si", method = RequestMethod.POST)
    public void checkUser(@ModelAttribute("userHandler") UserHandler userHandler, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        user = SIDAO.getUser(userHandler);
        if (user.getUsername() == null || user.getPassword() == null) {
            request.setAttribute("username", user.getUsername());
            request.getRequestDispatcher("LoginForm.jsp").forward(request, response);;
//            return mav;

        } else {
            request.setAttribute("username", user.getUsername());
            request.getRequestDispatcher("/WEB-INF/jsp/AdminDashboardNavbarAndNavigation.jsp").forward(request, response);

        }
    }

    @RequestMapping(value = "/saveImage.si", method = RequestMethod.POST)
    public ModelAndView saveImage(@RequestParam("file") MultipartFile[] files, @ModelAttribute("sIHandler") SaveImageHandler sIHandler, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                MultipartFile file = files[i];
                try {
                    byte[] bytes = file.getBytes();
                    // Creating the directory to store file
                    String rootPath = UPLOAD_DIRECTORY;
                    File dir = new File(rootPath + File.separator);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
                    //String URL = String.valueOf(serverFile);
                    sIHandler.setURL(file.getOriginalFilename());
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(bytes);
                    stream.flush();
                    stream.close();
                    SIDAO.saveImageURL(sIHandler);
//                    response.flushBuffer();
                } catch (Exception e) {
                }
            }
        }
        ModelAndView mav = new ModelAndView("AdminDashboardNavbarAndNavigation");
        return mav;
    }

    @RequestMapping(value = "/fetchSliderData.si", method = RequestMethod.GET)
    protected void fetchRightsList(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException, Exception {
        List<SaveImageHandler> sliderImagesList = SIDAO.FecthSliderList();
        String json = new Gson().toJson(sliderImagesList);
        res.setContentType("image/jpeg");
        res.setCharacterEncoding("UTF-8");
        res.getWriter().write(json);
        res.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/fetchImagesToDelete.si", method = RequestMethod.GET)
    protected void fetchImagesToDelete(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException, Exception {
        List<SaveImageHandler> sliderImagesList = SIDAO.FecthImagesToDelete();
        String json = new Gson().toJson(sliderImagesList);
        res.setContentType("image/jpeg");
        res.setCharacterEncoding("UTF-8");
        res.getWriter().write(json);
        res.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/DeleteSliderImage.si", method = RequestMethod.GET)
    protected void deleteSliderImage(@RequestParam(value = "id") String id, HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException, Exception {
        String fileName = id;
        try {
            File file = new File(UPLOAD_DIRECTORY + File.separator + fileName);
            SIDAO.DeleteSliderImage(fileName);
            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
        } catch (Exception e) {
            System.out.println("Failed to Delete image !!");
        }
//        List<RolesAndRightsHandler> RnRManagerRightsTypeList = RnRDao.RnRManagerAccessLevelTypeRecordList(id);
        String json = new Gson().toJson("true");
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        res.getWriter().write(json);
        res.flushBuffer();
        System.out.println(json);
    }

}
