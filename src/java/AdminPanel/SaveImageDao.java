/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdminPanel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nano Info 3
 */
public class SaveImageDao {

    public void saveImageURL(SaveImageHandler sIHandler) throws Exception {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");;
        }
        ResultSet rs = con.createStatement().executeQuery("select max(id) from ADMINPANDELSLIDERIMAGESTABLE");
        int mid = 0;
        if (rs.next()) {
            mid = rs.getInt(1);
            mid++;
        }
        PreparedStatement ps = con.prepareStatement("insert into ADMINPANDELSLIDERIMAGESTABLE values(?,?)");
        ps.setInt(1, mid);
        ps.setString(2, sIHandler.getURL());
        ps.executeUpdate();
        con.close();

    }

    public List<SaveImageHandler> FecthSliderList() throws Exception {

        List<SaveImageHandler> FecthSliderList = new ArrayList<SaveImageHandler>();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }

//        Statement stmnt  = con.createStatement();
        ResultSet rs = con.createStatement().executeQuery("select URL FROM ADMINPANDELSLIDERIMAGESTABLE order by ID asc ");
        while (rs.next()) {
            SaveImageHandler ImageSliderData = new SaveImageHandler();
            ImageSliderData.setURL(rs.getString(1));

            FecthSliderList.add(ImageSliderData);
        }
        con.close();

        return FecthSliderList;
    }

    public List<SaveImageHandler> FecthImagesToDelete() throws Exception {

        List<SaveImageHandler> FecthSliderList = new ArrayList<SaveImageHandler>();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }

//        Statement stmnt  = con.createStatement();
        ResultSet rs = con.createStatement().executeQuery("select URL FROM ADMINPANDELSLIDERIMAGESTABLE order by ID asc ");
        while (rs.next()) {
            SaveImageHandler ImageSliderData = new SaveImageHandler();
            ImageSliderData.setURL(rs.getString(1));

            FecthSliderList.add(ImageSliderData);
        }
        con.close();

        return FecthSliderList;
    }

    public void DeleteSliderImage(String fileName) throws Exception {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        PreparedStatement ps = con.prepareStatement("delete from ADMINPANDELSLIDERIMAGESTABLE where URL=?");
        ps.setString(1, fileName);
        ps.executeUpdate();
        con.close();
        System.out.println("Deleted");
    }

    public UserHandler getUser(UserHandler _userHandler) throws Exception {

        UserHandler uh = new UserHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        String USERNAME = _userHandler.getUsername();
        String PASSWORD = _userHandler.getPassword();

        ResultSet rs = con.createStatement().executeQuery("select * FROM  LFC_USERS WHERE USERNAME='" + USERNAME + "' AND PASSWORD='" + PASSWORD + "' ");
        while (rs.next()) {
            uh.setId(rs.getString(1));
            uh.setUsername(rs.getString(2));
            uh.setPassword(rs.getString(3));
        }
        con.close();

        return uh;
    }
}
