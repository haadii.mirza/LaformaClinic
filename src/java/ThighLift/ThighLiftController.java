/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThighLift;

import PostBariaticBodyLift.*;
import MommyMakeOver.*;
import LipoSuction.*;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class ThighLiftController {

    ThighLiftDao fDao = new ThighLiftDao();
    ThighLiftHandler fh;

    @RequestMapping(value = "/getThighLiftPageThighLiftIntro.tl", method = RequestMethod.GET)
    public void doGet(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_ThighLiftIntro(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getThighLiftPageThighLiftSecond.tl", method = RequestMethod.GET)
    public void ThighLiftSecond(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_ThighLiftSecond(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getThighLiftPageThighLiftThird.tl", method = RequestMethod.GET)
    public void ThighLiftThird(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_ThighLiftThird(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getThighLiftPageThighLiftFourth.tl", method = RequestMethod.GET)
    public void ThighLiftFourth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_ThighLiftFourth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getThighLiftPageThighLiftFifth.tl", method = RequestMethod.GET)
    public void ThighLiftFifth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_ThighLiftFifth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
}
