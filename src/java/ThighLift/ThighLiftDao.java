/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThighLift;

import PostBariaticBodyLift.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author Nano Info 3
 */
public class ThighLiftDao {

    public ThighLiftHandler getFace_ThighLiftIntro(String name) throws Exception {

        ThighLiftHandler fh = new ThighLiftHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public ThighLiftHandler getFace_ThighLiftSecond(String name) throws Exception {

        ThighLiftHandler fh = new ThighLiftHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public ThighLiftHandler getFace_ThighLiftThird(String name) throws Exception {

        ThighLiftHandler fh = new ThighLiftHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public ThighLiftHandler getFace_ThighLiftFourth(String name) throws Exception {

        ThighLiftHandler fh = new ThighLiftHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public ThighLiftHandler getFace_ThighLiftFifth(String name) throws Exception {

        ThighLiftHandler fh = new ThighLiftHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }
}
