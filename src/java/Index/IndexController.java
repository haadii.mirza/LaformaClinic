/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Index;

import AdminPanel.UserHandler;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class IndexController {

    IndexDao iDao = new IndexDao();
    IndexPageHandler IPH;

    @RequestMapping(value = "/getIndexPageContent.io", method = RequestMethod.GET)
    protected void doGet(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        IPH = iDao.getIndexContent(pageName);
        String json = new Gson().toJson(IPH);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getIndexTestimonialPageContent.io", method = RequestMethod.GET)
    protected void getIndexTestimonial(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        IPH = iDao.getIndexContent(pageName);
        String json = new Gson().toJson(IPH);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/editContent.io", method = RequestMethod.GET)
    protected void editContent(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
//        String pageName = name;
//        IPH = iDao.getIndexContent(pageName);
//        String json = new Gson().toJson(IPH);
//        response.setContentType("application/json");
//        response.getWriter().write(json);
//        //response.flushBuffer();
//        System.out.println(json);
        String pageName = name;
        IPH = iDao.gePageContentToEditByPagePortionName(pageName);
        request.setAttribute("id", IPH.getId());
        request.setAttribute("pageId", IPH.getPageId());
        request.setAttribute("name", IPH.getName());
        request.setAttribute("content", IPH.getContent());
        request.getRequestDispatcher("/WEB-INF/jsp/AdminEditContents.jsp").forward(request, response);
    }

    @RequestMapping(value = "/getIndexPagesList.io", method = RequestMethod.GET)
    protected void IndexPagesList(@RequestParam(value = "type") String type, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageType = type;
        List<IndexPageHandler> IndexPagesList = iDao.getIndexpagesList(pageType);
        String json = new Gson().toJson(IndexPagesList);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/updateContentById.io", method = RequestMethod.POST)
    protected void updateContentById(@ModelAttribute("IPHandler") IndexPageHandler IPHandler, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        iDao.updateContentById(IPHandler);
        request.getRequestDispatcher("/WEB-INF/jsp/AdminDashboardNavbarAndNavigation.jsp").forward(request, response);
    }
}
