/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Index;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nano Info 3
 */
public class IndexDao {

    public IndexPageHandler getIndexContent(String name) throws Exception {

        IndexPageHandler ih = new IndexPageHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            ih.setId(rs.getString(1));
            ih.setPageId(rs.getString(2));
            ih.setName(rs.getString(3));
            ih.setContent(rs.getString(4));
        }
        con.close();

        return ih;
    }

    public IndexPageHandler getIndexTestimonialContent(String name) throws Exception {

        IndexPageHandler ih = new IndexPageHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            ih.setId(rs.getString(1));
            ih.setPageId(rs.getString(2));
            ih.setName(rs.getString(3));
            ih.setContent(rs.getString(4));
        }
        con.close();

        return ih;
    }

    public List<IndexPageHandler> getIndexpagesList(String type) throws Exception {

        List<IndexPageHandler> ih = new ArrayList<IndexPageHandler>();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE PAGE_ID='" + type + "'");
        while (rs.next()) {
            IndexPageHandler handler = new IndexPageHandler();
            handler.setId(rs.getString(1));
            handler.setPageId(rs.getString(2));
            handler.setName(rs.getString(3));
            handler.setContent(rs.getString(4));
            ih.add(handler);
        }
        con.close();

        return ih;
    }

    public IndexPageHandler gePageContentToEditByPagePortionName(String name) throws Exception {

        IndexPageHandler ih = new IndexPageHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            ih.setId(rs.getString(1));
            ih.setPageId(rs.getString(2));
            ih.setName(rs.getString(3));
            ih.setContent(rs.getString(4));
        }
        con.close();

        return ih;
    }

    public void updateContentById(IndexPageHandler IPHandler) throws Exception {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }

        String ID = IPHandler.getId();
        PreparedStatement ps = con.prepareStatement("UPDATE PAGECONTENTS set CONTENT=? WHERE ID =" + ID + " ");

        ps.setString(1, IPHandler.getContent());

        ps.executeUpdate();
        con.close();
    }
}
