/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LipoSuction;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class LipoSuctionController {

    LipoSuctionDao fDao = new LipoSuctionDao();
    LipoSuctionHandler fh;

    @RequestMapping(value = "/getLipoSucPageLipoSucIntro.ls", method = RequestMethod.GET)
    public void doGet(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_LipoSucIntro(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getLipoSucPageLipoSucSecond.ls", method = RequestMethod.GET)
    public void LipoSucSecond(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_LipoSucSecond(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getLipoSucPageLipoSucThird.ls", method = RequestMethod.GET)
    public void LipoSucThird(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_LipoSucThird(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getLipoSucPageLipoSucFourth.ls", method = RequestMethod.GET)
    public void LipoSucFourth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_LipoSucFourth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
     @RequestMapping(value = "/getLipoSucPageLipoSucFifth.ls", method = RequestMethod.GET)
    public void LipoSucFifth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_LipoSucFifth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
}
