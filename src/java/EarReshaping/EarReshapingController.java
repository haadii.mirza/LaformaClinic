/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EarReshaping;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class EarReshapingController {

    EarReshapingDao fDao = new EarReshapingDao();
    EarReshapingHandler fh;

    @RequestMapping(value = "/getEarPageEarReshapeIntro.i", method = RequestMethod.GET)
    public void doGet(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFacEarReshapeIntro(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getEarPageEarReshapeSecond.i", method = RequestMethod.GET)
    public void ChinAugsecond(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFaceEarReshapeSecond(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getEarPageEarReshapeThird.i", method = RequestMethod.GET)
    public void ChinAugThird(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFaceEarReshapeThird(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getEarPageEarReshapeFourth.i", method = RequestMethod.GET)
    public void ChinAugFourth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFaceEarReshapeFourth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
}
