/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EarReshaping;

import ChinAugmentationController.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author Nano Info 3
 */
public class EarReshapingDao {

    public EarReshapingHandler getFacEarReshapeIntro(String name) throws Exception {

        EarReshapingHandler fh = new EarReshapingHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public EarReshapingHandler getFaceEarReshapeSecond(String name) throws Exception {

        EarReshapingHandler fh = new EarReshapingHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public EarReshapingHandler getFaceEarReshapeThird(String name) throws Exception {

        EarReshapingHandler fh = new EarReshapingHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public EarReshapingHandler getFaceEarReshapeFourth(String name) throws Exception {

        EarReshapingHandler fh = new EarReshapingHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }
}
