/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpecialOffers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author Nano Info 3
 */
public class SpecialOffersDao {

    public SpecialOffersHandler getFace_SpecialOffIntro(String name) throws Exception {

        SpecialOffersHandler fh = new SpecialOffersHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public SpecialOffersHandler getFace_SpecialOffSecond(String name) throws Exception {

        SpecialOffersHandler fh = new SpecialOffersHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public SpecialOffersHandler getFace_SpecialOffThird(String name) throws Exception {

        SpecialOffersHandler fh = new SpecialOffersHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public SpecialOffersHandler getFace_SpecialOffFourth(String name) throws Exception {

        SpecialOffersHandler fh = new SpecialOffersHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

}
