/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TummyTuc;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nano Info 3
 */
@org.springframework.stereotype.Controller
public class TummyTucController {

    TummyTucDao fDao = new TummyTucDao();
    TummyTucHandler fh;

    @RequestMapping(value = "/getTummyTucPageTummyTucIntro.tt", method = RequestMethod.GET)
    public void doGet(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_TummyTucIntro(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getTummyTucPageTummyTucSecond.tt", method = RequestMethod.GET)
    public void TummyTucSecond(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_TummyTucSecond(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getTummyTucPageTummyTucThird.tt", method = RequestMethod.GET)
    public void TummyTucThird(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_TummyTucThird(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getTummyTucPageTummyTucFourth.tt", method = RequestMethod.GET)
    public void TummyTucFourth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_TummyTucFourth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }

    @RequestMapping(value = "/getTummyTucPageTummyTucFifth.tt", method = RequestMethod.GET)
    public void TummyTucFifth(@RequestParam(value = "name") String name, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        String pageName = name;
        fh = fDao.getFace_TummyTucFifth(pageName);
        String json = new Gson().toJson(fh);
        response.setContentType("application/json");
        response.getWriter().write(json);
        //response.flushBuffer();
        System.out.println(json);
    }
}
