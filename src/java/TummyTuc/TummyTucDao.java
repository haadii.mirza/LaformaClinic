/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TummyTuc;

import ThighLift.*;
import PostBariaticBodyLift.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author Nano Info 3
 */
public class TummyTucDao {

    public TummyTucHandler getFace_TummyTucIntro(String name) throws Exception {

        TummyTucHandler fh = new TummyTucHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public TummyTucHandler getFace_TummyTucSecond(String name) throws Exception {

        TummyTucHandler fh = new TummyTucHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public TummyTucHandler getFace_TummyTucThird(String name) throws Exception {

        TummyTucHandler fh = new TummyTucHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public TummyTucHandler getFace_TummyTucFourth(String name) throws Exception {

        TummyTucHandler fh = new TummyTucHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }

    public TummyTucHandler getFace_TummyTucFifth(String name) throws Exception {

        TummyTucHandler fh = new TummyTucHandler();

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "hms", "nano");
        if (con != null) {
            System.out.println("Connected");
        }
        ResultSet rs = con.createStatement().executeQuery("select * FROM  PAGECONTENTS WHERE NAME='" + name + "'");
        while (rs.next()) {
            fh.setId(rs.getString(1));
            fh.setPageId(rs.getString(2));
            fh.setName(rs.getString(3));
            fh.setContent(rs.getString(4));
        }
        con.close();

        return fh;
    }
}
