
<html>
    <head>
        <title>Cheek Augmentation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/NON_SURGICAL_Cheek_Augmentation_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/cheekAugmentation.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Cheek Augmentation in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="cheek-aug"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivCheeckAugIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange"> Cheek Augmentation Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 "> 
                    <span class="myDivCheeckAugSecond"></span>
                    <div class="col-md-12 whole">
                        <img src="images/whole.png" alt="" width="60%" style="pointer-events: none; "/>
                    </div>
                    <span class="myDivCheeckAugThird"></span>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How long does it last?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivCheeckAugFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Recovery from Liquid Face Lift in Lahore</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivCheeckAugFifth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivCheeckAugSixth"> 
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
