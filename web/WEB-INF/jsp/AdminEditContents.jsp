<%-- 
    Document   : AdminEditContents
    Created on : Jul 2, 2018, 5:59:34 PM
    Author     : Nano Info 3
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script src="assests/vendor/AdminPanelAssests/js/jquery.min.js" type="text/javascript"></script>
<script src="assests/vendor/js/jquery1.1.js" type="text/javascript"></script>
<script src="assests/vendor/AdminPanelAssests/js/bootstrap.js" type="text/javascript"></script>
<script src="assests/vendor/AdminPanelAssests/js/adminlte.js" type="text/javascript"></script>
<script src="assests/js/AdminEditContents.js" type="text/javascript"></script>
<script src="assests/vendor/js/jquery-te-1.4.0.min.js" type="text/javascript"></script>

<link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="assests/vendor/AdminPanelAssests/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
<link href="assests/vendor/AdminPanelAssests/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="assests/vendor/AdminPanelAssests/css/ionicons.css" rel="stylesheet" type="text/css"/>
<link href="assests/vendor/AdminPanelAssests/css/_all-skins.css" rel="stylesheet" type="text/css"/>
<link href="assests/vendor/css/jquery-te-1.4.0.css" rel="stylesheet" type="text/css"/>
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
    div > .myCustomButton {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-77%, 97%);
        -ms-transform: translate(-50%, -50%);
        background-color: black;
        color: white;
        font-size: 16px;
        padding: 12px 24px;
        border: none;
        cursor: pointer;
        border-radius: 49px;
        text-align: center;
    }
    div > .mybtn {
        top: 50%;
        left: 50%;
        transform: translate(-77%, 97%);
        -ms-transform: translate(-50%, -50%);
        background-color: black;
        color: white;
        font-size: 16px;
        padding: 12px 24px;
        border: none;
        cursor: pointer;
        border-radius: 49px;
        text-align: center;
    }
</style>

<body class="hold-transition skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <!--<div class="wrapper">-->
    <div class="adminPanelHeader"></div>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <div class="adminPanelSideBar"></div>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="container" style="margin-top: 50px;">
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-12">
                        <form action="./updateContentById.io" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="${id}"/></input>
                            <input type="hidden" name="PageId" value="${pageId}"/></input>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <h4 for="colFormLabelSm" class="col-md-2 lb-sm">Name:</h4>
                                    <div class="col-md-10">
                                        <input type="text" name="name" class="form-control" id="colFormLabelSm" placeholder="" readonly="true" value="${name}"/></input>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <h4 for="colFormLabelSm" class="col-md-2 lb-sm">Content:</h4>
                                    <div class="col-md-10">
                                        <div  name="Content" id="Content">${content}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="col-md-5">

                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-default">Update</button>
                                    </div>
                                    <div class="col-md-5">

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</body>