<html>
    <head>
        <!--         jQuery library 
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
                </script>
                 Latest compiled and minified CSS 
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                 Latest compiled JavaScript 
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
                </script>-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/aboutPage.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/>
        <script> 
     $(function(){ $("#ourHeaderAbout").load("Headers/MasterHeader.jsp"); });
     $(function(){ $("#ourFooterAbout").load("Footers/MainFooter.jsp"); });
   </script>
    </head>
    <body>
        <div class="container first">
            <div id="ourHeaderAbout"></div>
         
        <!-- THIRD CONTAINER -->
        <div class="container third">
            <div class="col-md-12 third-header">
                <h2>SERVICES</h2>
            </div>

        </div>
        <div class="container fourth">
            <div class="col-md-5 fourth-l">
                <div class="col-md-12 fourth-left">
                    <h5>COSMETIC SURGERY IN LAHORE PAKISTAN:</h5>
                    <h6>NOSE RESHAPING</h6>
                    <h6>BREAST RESHAPING</h6>
                    <h6>LIPOSUCTION</h6>
                    <h6>TUMMY TUCK AND WEIGHT REDUCTION SURGERY</h6>
                    <h6>GYNACOMASTIA</h6>
                    <h6>FACE LIFT</h6>
                    <h6>EYELID SURGERY (BLEPHROPLASTY)</h6>
                    <h6>HAIR TRANSPLANTATION</h6>
                    <h6>FAT INJECTIONS</h6>
                    <h6>BOTOX, FILLERS & MICRODERMABRASION</h6>
                    <h6>LASERS</h6>
                    <h6>MOLE & SCAR TREATMENT</h6>
                    <h6>KELOIDS TREATMENT</h6>
                </div>
            </div>

            <div class="col-md-7 fourth-r">
                <div class="col-md-12 fourth-right">
                    <h5>RECONSTRUCTIVE SURGERY LAHORE PAKISTAN:</h5>
                    <h6>FACE TRAUMA</h6>
                    <h6>BURN RECONSTRUCTION</h6>
                    <h6>HAND SURGERY (TENDONS, NERVOUS, SOFT TISSUE,FRACTURES</h6>
                    <h6>MICROVASCULAR SURGERY & REPLANTATION OF HANDS & DIGITS</h6>
                    <h6>LOWER LIMB RECONSTRUCTION</h6>
                    <h6>DEGLOVING INJURIES</h6>
                    <h6>CLEFTLIP AND PALATE SURGERY</h6>
                    <h6>HEAD AND NECK SURGERY</h6>
                    <h6>SKIN CANCER SURGERY</h6>
                    <h6>BREAST RECONSTTRUCTION</h6>
                    <h6>BRACHIAL PLEXUS INJURY</h6>
                    <h6>BRACHIAL PLEXUS INJURY</h6>
                    <h6>BRACHIAL PLEXUS INJURY</h6>
                </div>
            </div>
        </div>
        <!-- FIFTH CONTAINER -->
        <div class="container fifth">
            <div class="row">
                <div class="col-md-12">
                    <p>The goal of cosmetic surgery is to improve a person?s appearance and, thus, self-esteem by changing the way she or he looks. Cosmetic surgery can be performed on any part of the face and body.
                    </p>
                </div>
            </div>
        </div>
        <!-- SIX CONTAINER -->
        <div class="container six">
            <div class="col-md-12">
                <h2>MANY PHYSICAL CHARACTERISTICS CAN BE SUCCESSFULLY CHANGED THROUGH COSMETIC SURGERY; OTHERS CANNOT. GOOD CANDIDATES FOR COSMETIC SURGERY:</h2>
                <ul>
                    <li>Have realistic expectations about what can be accomplished.</li>
                    <li>Understand the medical risks, physical effects during healing, how the surgery will affect them personally and 		
                        professionally,<br> what lifestyle changes may accompany the recovery period, and the expenses involved.</li>
                    <li>Have discussed their goalsfor surgery with their surgeon and resolved any questions.</li>
                    <li>Have diabetes or other chronic medical conditions under control.</li>
                    <li>Have no history of smoking or abstain from smoking (including secondhand smoke) and nicotine products, including
                        chewing tobacco and nicotine patches, gums or lozenges for six weeks before and after surgery.</li>
                    <li>Have had a stable weight for six months to one year.</li>
                </ul>

            </div>
        </div>
        <!-- SEVEN CONTAINER -->
        <div class="container seven">
            <div class="col-md-12">
                <h2>REASONS TO STOP SMOKING IF CONSIDERNG COSMETIC SURGERY:</h2>
                <ul>
                    <li>Nicotein, carbon monoxide and other toxins decrease blood flow to the skin.</li>
                    <li>Smoking effects would healing and worsens scarring.</li>
                    <li>Smoking increases the risk of post-anesthesia complications (pneumonia, blood clots, hypertension).</li>
                </ul>
            </div>
        </div>
        <!-- EIGHT CONTAINER -->
        <div class="container eight">
            <div class="col-md-12">
                <p>La?forma Plastic, Cosmetic and Hair Transplant Surgery Clinic.</p>
                <p>41-B, Kayaban-e-Firdousi, Johar Town, Lahore.</p>
                <p>For Consultation: 03009671600, 03018444358</p>
            </div>
        </div> 
        <div id="ourFooterAbout"></div>  
    </body>
</html>