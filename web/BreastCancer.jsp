<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Breast Cancer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BREAST_Breast_Cancer_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/breastCancer.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Breast Cancer surgery</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="breast-cancer"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivBreastCancerIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Breast Cancer Surgery Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastCancerSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is Breast Cancer Surgery performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastCancerThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastCancerFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation<br>for breast cancer with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastCancerFifth"> 
                    <br>
                    <p id="comment">Comments (5)</p>
                    <p id="purple-pp">Your email address will not be published. Required fields are marked *</p>
                    <div class="fill">
                        <input type="text" placeholder="FULLNAME" name=""><br>  
                        <input type="text" placeholder="EMAIL*" name=""> <br>
                        <input type="text" placeholder="PHONE NUMBER" name=""><br>
                    </div>
                    <textarea rows="10" cols="50">COMMENT</textarea><br>
                    <button id="btn">POST COMMENT</button>
                </div>
            </div>
            <div id="ourFooter"></div>
        </div
    </body>
</html>
