/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(function () {
        $("#ourHeader").load("Headers/MasterHeader.jsp");
    });
    $(function () {
        $("#ourFooter").load("Footers/MainFooter.jsp");
    });

    // ***INDEX **START **** fetching the content  
    var name = "Index";
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "/LaFormaClinic/getIndexPageContent.io?name=" + name + "",
        success: function (data) {
            console.log("success");
            console.log(data);
            //for (var i = 0; i < data.length; i++) {
                var dataContent = data.Content;
                $('.IndexContent').html('<p>"' + dataContent + '"</p>');
            //}
        },
        error: function () {
            console.log("failure");
        }
    });
    // ***INDEX **END **** fetching the content 


    // ***INDEXTESTIMONIAL **START **** fetching the content  
    var name = "IndexTestimonial";
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "/LaFormaClinic/getIndexTestimonialPageContent.io?name=" + name + "",
        success: function (data) {
            console.log("success");
            console.log(data);
            //for (var i = 0; i < data.length; i++) {
                var dataContent = data.Content;
                $('.IndexTestimonials').html('<p>"' + dataContent + '"</p>');
            //}
        },
        error: function () {
            console.log("failure");
        }
    });
    // ***INDEXTESTIMONIAL **END **** fetching the content 
});


