
<html>
    <head>
        <title>Lip Augmentation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/NON_SURGICAL_Lip_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/lip.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Lip Augmentation in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="lip"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivLipIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Lip Augmentation Lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is lip filling performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after the procedure?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipFourth">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation<br>for lip augmentation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipFifth">
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
