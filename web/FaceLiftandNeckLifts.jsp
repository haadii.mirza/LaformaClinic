<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>FaceLift and NeckLift</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/FACE_FaceLiftNeckLift_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/faceLiftsandneckLifts.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Necklift in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="face-neck"></div>
                <div class="row third-row">
                    <div class="col-md-12"> 
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <h2 id="orange">Neck lift in lahore?</h2>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-12"> 
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <h2 id="purple">How is Necklift performed?</h2>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-12"> 
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <h2 id="orange">What to expect after surgery?</h2>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-12"> 
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <h2 id="purple">Schedule a consultation for Necklift with Dr. Farrukh!</h2>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-12"> 
                    </div>
                </div>
                <div id="ourFooter"></div>
            </div>
    </body>
</html>
