<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Breast Augmentation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BREAST_Breast_Augmentation_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/breastAugmentation.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Breast augmentation with Fat injection</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="breast"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivBreastAugIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Breast Fat injection Lahore</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastAugSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is Breast Augmentation performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastAugThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastAugFourth"> 
                    <br>
                    <p id="purple-p">The following pictures were taken prior to and 6 weeks after our patient underwent breast
                        augmentation with the 24-hr recovery experience!</p>
                    <div class="col-md-12">
                        <img src="images/breast-pic.png" alt="" width="100%"/>
                    </div>
                    <div class="myDivBreastAugFifth"></div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation for<br>Breast Augmentation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivBreastAugSixth"> 
                    <br>
                    <p id="comment">Comments (5)</p>
                </div>

            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
