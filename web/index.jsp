<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title> Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/HomePageCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/> 
        <script>
            $(function () {
                $("#ourHeader").load("Headers/MasterHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>

        <!-- SECOND ROW SLIDER -->
        <div class="container second">

            <!-- THIRD ROW -->
            <div class="row third-row">
                <div class="col-md-1 col-xs-12"></div>
                <div class="col-md-2 cat col-sm-12 col-xs-12">
                    <div class="col-md-12">
                        <a href="BreastAugmentation.html"><div class="breast"></div></a>
                        <h5 id="purple">BREAST AUGMENTATION</h5>
                        <p>Implant or Fat injection</p>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12 cat">
                    <div class="col-md-12">
                        <a href="Liposuction.html"><div class="liposuction"></div></a>
                        <h5 id="org">LIPOSUCTION</h5>
                        <p>Liposuction & TummyTuc</p>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12 cat">
                    <div class="col-md-12">
                        <div class="male-breast"></div>
                        <h5 id="purple">MALE BEAST REDUCTION</h5>
                        <p>Gynecomastia treatment</p>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12 cat">
                    <div class="col-md-12">
                        <a href="Facelifts.html"><div class="facelift"></div></a>
                        <h5 id="org">FACELIFT & NECKLIFT</h5>
                        <p>Expert in facelift</p>
                    </div>
                </div>
                <div class="col-md-2 col-xs-12 cat">
                    <div class="col-md-12">
                        <div class="anti"></div>
                        <h5 id="purple">ANTI WRINKLE TREATMENT</h5>
                        <p>Fillers & Bottox</p>
                    </div>
                </div>
                <div class="col-md-1 col-xs-12"></div>
            </div>
            <!-- FOURTH ROW -->
            <div class="row fourth-row">
                <div class="col-md-12 fourth">
                    <h2>TOGETHER, WE CAN MAKE THE WHOLE WORLD SMILE</h2>
                    <div class="col-md-2 no-padding">
                        <div class="shape1"></div>
                        <div>
                            <h3 id="purple">FACE</h3>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="shape2"></div>
                        <div>
                            <h3 id="org">BREAST</h3>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="shape3"></div>
                        <div>
                            <h3 id="purple">BODY</h3>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="shape4"></div>
                        <div>
                            <h3 id="org">REVIEWS</h3>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="shape5"></div>
                        <div >
                            <h3 id="purple">GALLERY</h3>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="shape6"></div>
                        <div>
                            <h3 id="org">REVIEWS</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END FOURTH ROW -->
            <!-- START FIFTH ROW -->
            <div class="row fifth">
                <div class="col-md-4 fifth-shape"></div>
                <div class="col-md-8 fifth-text">
                    <h2>PLASTIC AND COSMETIC SURGERY, LAHORE</h2>
                    <h3>PLASTIC, COSMETIC AND HAIR TRANSPLANT, LAHORE</h3>
                    <br>
                    <div>
                        <p>DR.FARUKH ASLAM get asked, "Why should people invest in plastic surgery. As a firm believer that true
                            beauty comes from within,my answer inevitably revolves around the fact that if I can improve a
                            person's self esteem by enhancing the shape of their body, then their intrinsic beauty will shine
                            even brighter. To help you look AND feel your very best, I perform a full range pf cosmetic and
                            reconstructive procedures. Plastic surgery is highly individualized and rest assured that you will
                            recieve the time and attention you need to help determine the best procedures for you! "
                        </p>
                    </div>
                </div>
                <a href="Dr.Farrukh.html"><button id="btn">READ MORE</button></a>
            </div>
            <!-- END FIFTH ROW -->
            <!-- START SIX ROW -->
            <div class="row sixth">
                <div class="col-md-8 sixth-left">
                    <div class="row">
                        <div class="col-md-12 top">
                            <h1>MEET OUR DOCTORS</h1>
                        </div>
                        <div class="col-md-12 top1">
                            <h3>OUR MISSION IS YOUR HEALTH</h3>
                        </div>
                    </div>
                    <!-- <div class="col-lg-1 col-md-1 col-xs-6 col-sm-6"></div> -->
                    <div class="col-md-4 left">
                        <h4>PEDIATRIC SURGEON</h4>
                        <div class="left-shape"></div>
                        <h5>DR.HASNAIN ABBAS</h5>
                    </div>
                    <div class="col-md-4 middle">
                        <h4>DENTIST</h4>
                        <div class="middle-shape"></div>
                        <h5>DR.FAHEEM ABRAR</h5>
                    </div>
                    <div class="col-md-4 right">
                        <h4>DENTIST</h4>
                        <div class="right-shape"></div>
                        <h5>DR.WAJIHA ALAMGEER </h5>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <a href="OurStaff.html"><button id="more">MORE DOCTORS</button></a>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
                <div class="col-md-4 sixth-right">
                    <div class="col-md-12 s-right">
                        <h4>MAKE AN APPOINTMENT</h4>
                        <h3>+92 300 3446070</h3>
                        <h5>OR</h5>
                        <input type="text" placeholder="FULLNAME" name="">
                        <input type="text" placeholder="SUBJECT" name="">
                        <input type="text" placeholder="PHONE NUMBER" name="">
                        <input type="text" placeholder="EMAIL ADDRESS" name="">
                        <input type="text" placeholder="APPOINTMENT DATE" name="">
                        <input type="text" placeholder="MESSAGE" name="">
                        <div class="row">
                            <div class="col-md-12">
                                <button id="request">SUBMIT REQUEST</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SIXTH ROW -->
            <!-- START SEVENTH ROW -->
            <div class="row seventh-row">
                <div class="col-md-12">
                    <div class="col-md-8 seventh-left">
                        <div class="row">
                            <div class="col-md-12 heading">
                                <h2>TESTIMONIALS</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p> "I had a PRP treatment session at La\'forma Clinic, Lahore to stop hair fall
                                    and held <br>re-grow some new hair. The treatment was administered by Dr. Farrukh
                                    Aslam. I ho-<br>nestly appreciate the highly professional and caring handling of patients
                                    at the clinic.<br>Dr.Farrukh is indeed a dedicated medical professional who aims to
                                    give priority to the<br>benefit of his patients and clients. On my next visit to Pakistan from
                                    Qatar, I will surely<br>have another session of PRP hair treament at La'\forma Clinic."
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Muhammad Athar Shah, Qatar University</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 seventh-right">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>OUR AFFILIATIONS</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 seventh-signs">
                                        <img src="images/1.gif">
                                    </div>
                                    <div class="col-md-6 seventh-signs">
                                        <img src="images/2.gif">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 seventh-signs">
                                        <img src="images/3.jpg">
                                    </div>
                                    <div class="col-md-6 seventh-signs">
                                        <img src="images/4.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END SEVENTH -->
            <!-- START EIGHT -->
            <div class="row eight-row">
                <div class="col-md-12">
                    <div class="col-md-4 eight-left">
                        <img src="images/5.jpg">
                    </div>
                    <div class="col-md-4 eight-middle">
                        <h1>FLY FOR<br>IN SURGERY</h1>
                    </div>
                    <div class="col-md-4 eight-right">
                        <p>Many of our patients fly in from all over theworld. We have perfected
                            the "fly-in patient"protocol, making it virtually possible for all
                            prospective patients. Fly-in alone or with lovedones, and we will walk you
                            through a processthat will ensure complete care and acomfortable, pleasant experience.
                        </p>
                    </div>
                </div>
            </div>
            <!-- END EIGHT ROW -->
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
