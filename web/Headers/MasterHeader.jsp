<div class="container first">
    <div class="row">
        <!-- <div class="contact">
           <img src="images/fb.png">
           <img src="images/twitter.png">
           <img src="images/gmail.png">
           <h2 class="heading-overlay">+923003446070</h2>
           </div> -->
        <div class="col-md-1 first-left">
            
            <a href="index.jsp">
                <img  src="images/LOGO.png" height="75px"> 
            </a>
        </div>
        <div class="col-md-11 first-right">
            <nav class="navbar navbar-default">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <div class="collapse navbar-collapse " id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.jsp">HOME</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="About.jsp">ABOUT</a></li>
                                <li><a href="Dr.Farrukh.jsp">DR FARRUKH</a></li>
                                <li><a href="OurStaff.jsp">OUR STAFF</a></li>
                                <li><a href="OurBlog.jsp">OUR BLOG</a></li>
                                <li><a href="Testimonials.jsp">TESTIMONIALS</a></li>
                                <li><a href="PrivacyPolicy.jsp">PRIVACY POLICY</a></li> 
                            </ul>
                        </li>
                        <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">FACE
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="BrowLift.jsp">BROWLIFT</a></li>
                                <li><a href="ChinAugmentation.jsp">CHIN AUGMENTATION</a></li>
                                <li><a href="EarReshaping.jsp">EAR RESHAPING</a></li>
                                <li><a href="EyelidSurgery.jsp">EYELIED SURGERY</a></li>
                                <li><a href="Facelifts and Necklifts.jsp">FACELIFT</a></li>
                                <li><a href="FacialFatGrafting.jsp">FACIAL FAT GRAFTING</a></li>
                                <li><a href="LipAugmentation.jsp">LIP AUGMANTATION</a></li>
                                <li><a href="LipReduction.jsp">LIP REDUCTION</a></li>
                                <li><a href="FaceLiftandNeckLifts.jsp">FACELIFT AND NECKLIFT</a></li>
                                <li><a href="NoseReshaping.jsp">NOSE RESHAPInG</a></li>
                            </ul></li> 
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">BODY
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="ArmsLift.jsp">ARMS LIFT (Brachioplasty)</a></li>
                                <li><a href="ButtockAugmentation.jsp">BUTTOCK AUGMANTATION</a></li>
                                <li><a href="LipoSculpturing.jsp">LIPOSCULPTURING</a></li>
                                <li><a href="Liposuction.jsp">LIPOSUCTION</a></li>
                                <li><a href="MommyMakeover.jsp">MOMMY MAKEOVER</a></li>
                                <li><a href="Post-bariatric-body-Lift.jsp">POST BRAIATRIC BODY LIFT</a></li>
                                <li><a href="ThighLift.jsp">THIGH LIFT</a></li>
                                <li><a href="TummyTuc.jsp">TUMMY TUC (Abdominoplasty)</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">BREAST
                               <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="BreastAugmentation.jsp">BREAST AUGMENTATION WITH FAT INJECTION</a></li>
                                <li><a href="BreastImplant.jsp">BREAST AUGMENTATION WITH IMPLANT</a></li>
                                <li><a href="BreastCancer.jsp">BREAST CANCER SURGERY</a></li>
                                <li><a href="BreastLift.jsp">BREAST LIFT</a></li>
                                <li><a href="BreastReconstruction.jsp">BREAST RECONSTRUCTION</a></li>
                                <li><a href="BreastReduction.jsp">BREAST REDUCTION</a></li>
                                <li><a href="CorrectionNipples.jsp">CORRECTION OF INVERTED NIPPLES</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">MALE
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="HairTransplant.jsp">HAIR TRANSPLANT</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">NON-SURIGICAL
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="Botox.jsp">BOTOX</a></li>
                                <li><a href="CheekAugmentation.jsp">CHEEK AUGMENTATION</a></li>
                                <li><a href="ChemicalPeels.jsp">CHEMICAL PEELS</a></li>
                                <li><a href="Chin.jsp">CHIN AUGMENTATION</a></li>
                                <li><a href="Fillers.jsp">FILLERS</a></li>
                                <li><a href="Lip.jsp">LIP AUGMENTATION</a></li>
                                <li><a href="Piercing.jsp">PIERCING</a></li>
                                <li><a href="PRP.jsp">PRP THERAPY</a></li>
                                <li><a href="Tattooing.jsp">TATTOOING</a></li>
                                
                                
                            </ul>
                        </li>
                         <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">OFFERS
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="Events.jsp">EVENTS</a></li>
                                <li><a href="MedicalTourism.jsp">MEDICAL TOURISM</a></li>
                                <li><a href="SpecialOffers.jsp">SPEICAL OFFERS</a></li>
                            </ul>
                        </li>
                        <li><a href="ContactUs.jsp">CONTACT US</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="container">
    <div class="row second-row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/slider1.png" alt="" >
                </div>
                <div class="item">
                    <img src="images/slider2.png" alt="" >
                    <div class="carousel-caption carousel-css">
                    </div>
                </div>
                <div class="item">
                    <img src="images/slider3.png" alt="">
                </div>
                <div class="item">
                    <img src="images/Dr. Farrukh.png" alt=""/>
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
            <!-- <h2 style="position: relative;top: -300px; left: -300px; text-align: center; color: purple;background-color: white">Breast AUgmentation</h2> -->
        </div>
    </div>
</div>
