<div class="container first">
    <div class="row">
        <!-- <div class="contact">
           <img src="images/fb.png">
           <img src="images/twitter.png">
           <img src="images/gmail.png">
           <h2 class="heading-overlay">+923003446070</h2>
           </div> -->
        <div class="col-md-1 first-left">
            <a href="index.jsp">
                <img  src="images/LOGO.png" height="75px">
            </a>
        </div>
        <div class="col-md-11 first-right">
            <nav class="navbar navbar-default">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
               <div class="collapse navbar-collapse " id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.jsp">HOME</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="About.jsp">About</a></li>
                                <li><a href="Dr.Farrukh.jsp">Dr Farrukh</a></li>
                                <li><a href="OurStaff.jsp">Our Staff</a></li>
                                <li><a href="OurBlog.jsp">Our Blog</a></li>
                                <li><a href="Testimonials.jsp">Testimonials</a></li>
                                <li><a href="PrivacyPolicy.jsp">Privacy Policy</a></li> 
                            </ul>
                        </li>
                        <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">FACE
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="BrowLift.jsp">BrowLift</a></li>
                                <li><a href="ChinAugmentation.jsp">Chin Augmentation</a></li>
                                <li><a href="EarReshaping.jsp">Ear Reshaping</a></li>
                                <li><a href="EyelidSurgery.jsp">Eyelid Surgery</a></li>
                                <li><a href="Facelifts.jsp">Facelift</a></li>
                                <li><a href="FacialFatGrafting.jsp">Facial Fat Grafting</a></li>
                                <li><a href="LipAugmentation.jsp">Lip Augmentation</a></li>
                                <li><a href="LipReduction.jsp">Lip Reduction</a></li>
                                <li><a href="FaceLiftandNeckLifts.jsp">FaceLift and NeckLift</a></li>
                                <li><a href="NoseReshaping.jsp">Nose Reshaping</a></li>
                            </ul></li> 
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">BODY
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="ArmsLift.jsp">Arms Lift (Brachioplasty)</a></li>
                                <li><a href="ButtockAugmentation.jsp">Buttock Augmentation</a></li>
                                <li><a href="LipoSculpturing.jsp">Liposculpturing</a></li>
                                <li><a href="Liposuction.jsp">Liposuction</a></li>
                                <li><a href="MommyMakeover.jsp">Mommy Makeover</a></li>
                                <li><a href="Post-bariatric-body-Lift.jsp">Post bariatric body Lift</a></li>
                                <li><a href="ThighLift.jsp">Thigh Lift</a></li>
                                <li><a href="TummyTuc.jsp">Tummy Tuc (Abdominoplasty)</a></li>
                            </ul>
                        </li>
                         <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">BREAST
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="BreastAugmentation.jsp">BREAST AUGMENTATION WITH FAT INJECTION</a></li>
                                <li><a href="BreastImplant.jsp">BREAST AUGMENTATION WITH IMPLANT</a></li>
                                <li><a href="BreastCancer.jsp">BREAST CANCER SURGERY</a></li>
                                <li><a href="BreastLift.jsp">BREAST LIFT</a></li>
                                <li><a href="BreastReconstruction.jsp">BREAST RECONSTRUCTION</a></li>
                                <li><a href="BreastReduction.jsp">BREAST REDUCTION</a></li>
                                <li><a href="CorrectionNipples.jsp">CORRECTION OF INVERTED NIPPLES</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">MALE
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="HairTransplant.jsp">Hair Transplant</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">NON-SURIGICAL
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="Botox.jsp">BOTOX</a></li>
                                <li><a href="CheekAugmentation.jsp">CHEEK AUGMENTATION</a></li>
                                <li><a href="ChemicalPeels.jsp">CHEMICAL PEELS</a></li>
                                <li><a href="Chin.jsp">CHIN AUGMENTATION</a></li>
                                <li><a href="Fillers.jsp">FILLERS</a></li>
                                <li><a href="Lip.jsp">LIP AUGMENTATION</a></li>
                                <li><a href="Piercing.jsp">PIERCING</a></li>
                                <li><a href="PRP.jsp">PRP THERAPY</a></li>
                                <li><a href="Tattooing.jsp">TATTOOING</a></li>
                                
                                
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#">OFFERS
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu SubMenu">
                                <li><a href="Events.jsp">EVENTS</a></li>
                                <li><a href="MedicalTourism.jsp">MEDICAL TOURISM</a></li>
                                <li><a href="SpecialOffers.jsp">SPEICAL OFFERS</a></li>
                            </ul>
                        </li>
                        <li><a href="ContactUs.jsp">CONTACTS US</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>