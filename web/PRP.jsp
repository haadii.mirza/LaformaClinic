
<html>
    <head>
        <title>PRP Surgery</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/NON_SURGICAL_Prp_Theropy_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/prp.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>PRP surgery</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="prp"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivPrpIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">PRP lahore</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12"> 
                    <span class="myDivPrpSecond">
                        
                    </span>
                    <p>It is effective for number of conditions</p>
                    <div class="col-md-12 whole">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <img src="images/Hair-loss.png" alt="" width="100%"/>
                        </div>
                        <div class="col-md-2">
                            <img src="images/Acne-scars.png" alt="" width="100%"/>
                        </div>
                        <div class="col-md-2">
                            <img src="images/Burn-scar.png" alt="" width="100%"/>
                        </div>
                        <div class="col-md-2">
                            <img src="images/Photo-ageing.png" alt="" width="100%"/>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is PRP in lahore is performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivPrpThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivPrpFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation for<br>arm lift in lahore with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivPrpFifth"> 

                    <hr>
                    <p id="comment">Leave a Reply</p>
                    <p id="purple-pp">Your email address will not be published. Required fields are marked *</p>
                    <div class="fill">
                        <input type="text" placeholder="NAME*" name=""><br>  
                        <input type="text" placeholder="EMAIL*" name=""> <br>
                        <input type="text" placeholder="WEBSITE" name=""><br>
                    </div>
                    <textarea rows="10" cols="50">COMMENT</textarea><br>
                    <button id="btn">POST COMMENT</button>
                </div>
            </div>
            <div id="ourFooter"></div>
        </div
    </body>
</html>
