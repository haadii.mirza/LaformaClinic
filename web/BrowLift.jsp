
<html>
    <head>
        <title>Browlift</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/FACE_BrowLift_ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/browLift.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Brow lift in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="browlift"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivFaceLiftIntro">

                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">BROW LIFT LAHORE PAKISTAN?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFaceLiftSecond">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">HOW IS BROW LIFT PERFORMED?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFaceLiftThird">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">WHAT TO EXPECT AFTER SURGERY?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFaceLiftFourth">
                  
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation for brow lift with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFaceLiftFifth">
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
