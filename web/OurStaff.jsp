<%-- 
    Document   : OurStaff
    Created on : Jun 22, 2018, 11:31:50 AM
    Author     : Nano Info 3
--%>
<html>
    <head>
        <title>Our Doctors</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/MasterHeaderCss_1.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/ourStaff.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#navBarHeader").load("Headers/MasterHeader.jsp");
            });
            $(function () {
                $("#ourDoctorsFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="navBarHeader"></div>
        <!--        <div class="container second">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="f1">
        
                                <img src="images/OurDoctors(link).png" class="b"> 
                            </div>
                        </div>
                    </div>
        
                </div>-->
        <div class="container third">
            <div class="col-md-12 third-b">
                <div class="col-md-3 left">
                    <h5>PEDIATRIC SURGEON</h5>
                    <div class="left-shape"></div>
                    <h4>DR.HASNAIN ABBAS</h4>
                    <button>MORE</button>
                </div>
                <div class="col-md-3 middle1">
                    <h5>DENTIST</h5>
                    <div class="middle1-shape"></div>
                    <h4>DR.FAHEEM ABRAR</h4>
                    <button>MORE</button>
                </div>
                <div class="col-md-3 middle2">
                    <h5>DENTIST</h5>
                    <div class="middle2-shape"></div>
                    <h4>DR.WAJIHA ALAMGEER</h4>
                    <button>MORE</button>
                </div>
                <div class="col-md-3 right">
                    <h5>PEDIATRIC SURGEON</h5>
                    <div class="right-shape"></div>
                    <h4>AISHA ALAM</h4>
                    <button>MORE</button>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="ourDoctorsFooter"></div>
        </div>


    </body>
</html>
