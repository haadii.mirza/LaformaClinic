<html>
    <head>
        <title>Fillers</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/NON_SURGICAL_Fillers_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/fillers.css" rel="stylesheet" type="text/css"/>
        <script> 
$(function(){ $("#ourHeader").load("Headers/NavBarHeader.jsp"); });
     $(function(){ $("#ourFooter").load("Footers/MainFooter.jsp"); });

   </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Fillers in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="fillers"></div>
            <div class="row third-row">
                <div class="col-md-12 myDivFillersIntro">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Facial Fillers Lahore?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFillersSecond">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is the  procedure performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFillersThird">
            </div>
             
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFillersFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivFillersFifth">    
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
