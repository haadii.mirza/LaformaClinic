<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Liposculpturing</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/lipoSculpturing.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Liposculpturing in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="lipo"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12">
                    <p id="center">The details of the page will soon be updated..</p>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Schedule a consultation with Dr. Farrukh!</h2>
                </div>

                <div class="col-md-12">
                    <p id="text">Are you interested in procedure? Please contact Dr. Farrukh’s office to book your consultation
                        by calling (92) 42 35236070, (92) 300 3446070 or submitting an online contact request today.
                        During your consultation with Dr. Farrukh, he will be happy to address any questions or concerns
                        you may have. Our office is conveniently located at 41-B, Kayaban-e-Firdousi, Johar Town, Lahore.</p>
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
