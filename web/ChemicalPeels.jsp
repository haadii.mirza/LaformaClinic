<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Chemical Peels</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/NON_SURGICAL_Chemical_Peels_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/chemicalPeels.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Chemical Peel in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="chemical"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivChemPeelsIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What is Chemical peel?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivChemPeelsSecond"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is chemical peel performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivChemPeelsThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What to expect after peel?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivChemPeelsFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation with Dr. Farrukh!!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivChemPeelsFifth"> 

                    <hr>
                    <p id="comment">Comments (5)</p>
                    <p id="purple-pp">Your email address will not be published. Required fields are marked *</p>
                    <div class="fill">
                        <input type="text" placeholder="FULLNAME" name=""><br>  
                        <input type="text" placeholder="EMAIL*" name=""> <br>
                        <input type="text" placeholder="PHONE NUMBER" name=""><br>
                    </div>
                    <textarea rows="10" cols="50">COMMENT</textarea><br>
                    <button id="btn">POST COMMENT</button>
                </div>
            </div>
            <div id="ourFooter"></div>
        </div
    </body>
</html>
