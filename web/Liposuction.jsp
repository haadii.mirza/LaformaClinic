<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Liposuctions</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/BODY_Lipo_Suction_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/liposuction.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });
        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Liposuction in Lahore, Pakistan</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="liposuction"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivLipoSucIntro"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">What is Liposuction in lahore?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipoSucSecond"> 
                </div>
            </div>
            <div class="row fourth-row">
                <div class="col-md-1"></div>
                <div class="col-md-2  ">
                    <img class="image" src="images/cir-1.png" alt="" width="170px" />
                </div>
                <div class="col-md-2">
                    <img class="image" src="images/cir-2.png" alt="" width="170px"/>
                </div>
                <div class="col-md-2 ">
                    <img class="image" src="images/cir-3.png" alt="" width="170px"/>
                </div>
                <div class="col-md-2">
                    <img  class="image" src="images/cir-4.png" alt="" width="170px"/>
                </div>
                <div class="col-md-2 ">
                    <img class="image" src="images/cir-5.png" alt="" width="170px"/>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">How is Liposuction in Lahore performed?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipoSucThird"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange"> What to expect after surgery?</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipoSucFourth"> 
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Schedule a consultation for<br>liposuction in Lahore with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivLipoSucFifth"> 
                </div>
            </div>
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
