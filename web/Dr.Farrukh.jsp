<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Dr.Farrukh</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/ABOUT__Dr_farrukh_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/dr.farrukh.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <!--START FIRST ROW-->
            <div class="row">
                <div class="col-md-8 f-left">
                    <div class="col-md-12 first-row-left">
                        <h2>Consultant Plastic, Cosmetic and Hand Surgeon</h2>
                        <p>Jinnah Burn and Reconstructive Surgery Centre, Lahore<br>
                            Clinic: 41-B, Khayabane Firdousi, Johar Town
                        </p>
                    </div>
                </div>
                <div class="col-md-4 f-right ">
                    <div class="col-md-12 first-row-right">
                        <h2>VISTING CONSULTANT:</h2>
                        <p>Shalamar Hospital, Lahore<br>
                            Aadil Hospital, DHA, Lahore<br>
                            Doctors Hospital, Lahore<br>
                            www.laformaclinic.com
                        </p>
                    </div>
                </div>
            </div>
            <!--END FIRST ROW-->
            <!--START SECOND ROW-->
            <div class="row second">
                <div class="col-md-12 second-row">
                    <p>TYPES OF PROCEDURES HE OFFERED ARE</p>
                </div>
            </div>
            <!--END SECOND ROW-->
            <!--START THIRD ROW-->
            <div class="row third">
                <div class="col-md-8 t-left">
                    <div class="col-md-12 third-row-left">
                        <h2>THE FACE</h2>
                        <ul class="left-list">
                            <li>Botox</li>
                            <li>Cheek Lift</li>
                            <li>Chemical peel</li>
                            <li>Chin Surgery</li>
                            <li>Cosmetic Dentistry</li>
                            <li>Dermabrasion</li>
                            <li>Eyebrow/Forehead Rejuvenation (Brow Lift)</li>
                            <li>Eyelied Surgery (Blepharoplasty)</li>
                            <li>Face Lift</li>
                            <li>Facial Contouring</li>
                            <li>Facial Fillers</li>
                            <li>Facial Wrinkles</li>
                            <li>Laser Hair Removal</li>
                            <li>Laser Resurfacing</li>
                            <li>Neck Lift and Neck Liposuction</li>
                            <li>Otoplasty (Alterations of the Ears)</li>
                            <li>Rhinoplasty (Alterations of the Nose)</li>
                            <li>Skin Problems (Blemishes, Spider, Veins, Scar Revisions, Tattoo Removal)</li>
                            <li>Wrinkle Treatment</li>  
                        </ul>    
                    </div>
                </div>
                <div class="col-md-4 t-right">
                    <div class="col-md-12 third-row-right">
                        <h2>Questions for the patient<br>to consider before<br>pursuing cosmetic<br>surgery:</h2>
                        <ul class="right-list">
                            <li>What are my motives for wanting to change how I look?</li>
                            <li>What are the specific attributes of my appearance that I want to change?</li>
                            <li>Do I have realistic expectations about the results of the surgery?</li>
                            <li>What aspects of my life will be affected such as family, work, travel and social obligations</li>
                            <li>What time in my life will work best so that I have the greatest chance for a successful recovery?</li>
                            <li>Have I talked about my concerns and questions openly with my health care provider?</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--END THIRD ROW-->
            <!--START FOURTH ROW-->
            <div class="row fourth">
                <div class="col-md-5 fourth-left">
                    <div class="col-md-12 fourth-row-left">
                        <h2>THE BODY</h2>
                        <ul class="fourth-left-list">
                            <li>Abdomen Reduction (Tummy Tuck)</li>
                            <li>Arm Lift</li>
                            <li>Body Liposuction</li>
                            <li>Breast Augmentation</li>
                            <li>Breast Lift</li>
                            <li>Breast Reduction</li>
                            <li>Buttock Lift (Belt Lipectomy</li>
                            <li>Circumferential Body Lift</li>
                            <li>Inner Tight Lift</li>
                            <li>Laser Hair Removal</li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-7 f-right">
                    <div class="col-md-12 fourth-row-right myDivScheduleConsultant">

                    </div>
                </div>
            </div>
            <!--END FOURTH ROW-->
            <div id="ourFooter"></div>
        </div>
    </body>
</html>
