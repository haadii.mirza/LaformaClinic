
<html>
    <head>
        <title>Special Offers</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="assests/vendor/js/jquery.min.js" type="text/javascript"></script>
        <script src="assests/vendor/js/bootstrap.js" type="text/javascript"></script>
        <script src="assests/js/OFFERS_SpeicalOffers_Ajax_Requests.js" type="text/javascript"></script>
        <link href="assests/vendor/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/footerCss_1.css" rel="stylesheet" type="text/css"/> 
        <link href="assests/css/MasterHeaderCss.css" rel="stylesheet" type="text/css"/>
        <link href="assests/css/specialOffers.css" rel="stylesheet" type="text/css"/>
        <script>
            $(function () {
                $("#ourHeader").load("Headers/NavBarHeader.jsp");
            });
            $(function () {
                $("#ourFooter").load("Footers/MainFooter.jsp");
            });

        </script>
    </head>
    <body>
        <div id="ourHeader"></div>
        <div class="container first">
            <div class="row first-row">
                <div class="col-md-12 heading">
                    <h2>Special Offer in Hair Transplant and Cosmetic Surgery</h2>
                </div>
            </div>
            <div class="row second-row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <img src="images/special1.png" alt="" width="100%"/>
                </div>
                <div class="col-md-3"></div>

                <div class="col-md-12 offer"></div>
            </div>
            <div class="row third-row">
                <div class="col-md-12 myDivSpecialOffIntro">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">Hair Transplant Ramadan offer</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivSpecialOffSecond">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Non Surgical weight loss, lose upto 30 kg in 6 months</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12 myDivSpecialOffThird">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="purple">special offers cosmetic<br>hair transplantsurgery lahore Pakistan</h2>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-12"></div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2 id="orange">Schedule a consultation with Dr. Farrukh!</h2>
                </div>
                <div class="col-md-2"></div>

                <div class="col-md-12 myDivSpecialOffSecond">
                </div>
            </div>
            <div id="ourFooter"></div>

        </div>

    </div>
</body>
</html>

